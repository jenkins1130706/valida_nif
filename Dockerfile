FROM python:3.11-slim

RUN useradd -m user
USER user
WORKDIR /home/user
ENV PATH="/home/user/.local/bin:${PATH}"
COPY --chown=user:user requirements.txt .
RUN pip install --user --upgrade pip
RUN pip install --user -r requirements.txt

COPY --chown=user:user . .
EXPOSE 9046

CMD ["python", "app.py"]