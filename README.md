# NIF VALIDATOR in Python

## Build

    docker build -t nif_validator .

## Local run

    docker run -d --name nif_validator p80:9046 nif_validator

## Jenkins Gitlab integration

* Create a gitlab token (Personal tokens)
* Add Jenkins gitlab plugin
* Create project
* Trigger the build
